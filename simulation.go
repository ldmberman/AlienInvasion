package main

import (
	"fmt"
	"math/rand"
	"log"
	"strings"
)

type AlienSet map[string]*AlienStatus

type AlienStatus struct {
	Moved int
	Command chan AlienCommand
}

func Fight(cityLists map[City][]string, activeAliens AlienSet, m Map) {
	for city, arrived := range cityLists {
		if len(arrived) < 2 {
			continue
		}

		// Remove the city from the map.
		delete(m, city)
		// Destroy the aliens.
		for _, alien := range arrived {
			activeAliens[alien].Command <- AlienDestroy
			delete(activeAliens, alien)
		}
		// Remove the roads.
		for c, cityMap := range m {
			if cityMap.North != nil && *cityMap.North == city {
				cityMap.North = nil
				m[c] = cityMap
			}
			if cityMap.East != nil && *cityMap.East == city {
				cityMap.East = nil
				m[c] = cityMap
			}
			if cityMap.West != nil && *cityMap.West == city {
				cityMap.West = nil
				m[c] = cityMap
			}
			if cityMap.South != nil && *cityMap.South == city {
				cityMap.South = nil
				m[c] = cityMap
			}
		}
		fmt.Printf("%s has been destroyed by aliens: %s.\n", city, strings.Join(arrived, ", "))
	}
}

func Simulation(m Map, n int64) {
	// Keep track of the current status of every alien.
	var alienSet = AlienSet{}
	// Keep track of how many aliens visit each city.
	var cityLists = map[City][]string{}

	// Generate n aliens in the random places building up initial arrival lists and alien statuses.
	var cities []City
	for city := range m {
		cities = append(cities, city)
	}
	radio := make(chan AlienResponse)
	for i := int64(0); i < n; i++ {
		in := make(chan AlienCommand)
		id := fmt.Sprintf("%d", i)

		city := cities[rand.Int() % len(cities)]
		pick := m[city]
		go Alien(id, pick, m, in, radio)
		alienSet[id] = &AlienStatus{Command: in}
		appendCity(cityLists, city, id)
	}

	// Aliens fight - the cities with more than one alien are destroyed, together with
	// the roads and aliens.
	Fight(cityLists, alienSet, m)
	if len(alienSet) == 0 {
		fmt.Println("All aliens are immediately dead. Ending simulation..")
		printMap(m)
		return
	}

	// Start turn-based simulation.
	Turns:
	for {
		// Prepare the fresh arrival list.
		cityLists = map[City][]string{}

		// Make each alien make a turn.
		for _, alien := range alienSet {
			alien.Command <- AlienTurn
		}

		// Update the record of every tracked alien and city.
		var trapped []string
		for i := 0; i < len(alienSet); i++ {
			resp := <-radio
			switch resp.Response {
			case AlienMoved:
				alienSet[resp.ID].Moved++
				appendCity(cityLists, *resp.MovedTo, resp.ID)
			case AlienTrapped:
				alienSet[resp.ID].Command <- AlienDestroy
				trapped = append(trapped, resp.ID)
			default:
				log.Fatalf("Got unexpected response: %s", resp.Response)
			}
		}
		for _, alien := range trapped {
			delete(alienSet, alien)
		}

		Fight(cityLists, alienSet, m)

		if len(alienSet) == 0 {
			fmt.Println("All aliens are either dead or trapped. Ending simulation..")
			printMap(m)
			return
		}
		for _, status := range alienSet {
			if status.Moved < 10000 {
				continue Turns
			}
		}
		fmt.Println("Those aliens that are not dead or trapped each made more than 10000 moves. Ending simulation..")
		printMap(m)
		return
	}
}

func appendCity(cityLists map[City][]string, city City, id string) {
	if _, exists := cityLists[city]; !exists {
		cityLists[city] = []string{id}
	} else {
		cityLists[city] = append(cityLists[city], id)
	}
}

func printMap(m Map) {
	fmt.Println("\nWhat's left of the map:")
	for c, cityMap := range m {
		var roads string
		if cityMap.North != nil {
			roads += " north=" + string(*cityMap.North)
		}
		if cityMap.South != nil {
			roads += " south=" + string(*cityMap.South)
		}
		if cityMap.West != nil {
			roads += " north=" + string(*cityMap.West)
		}
		if cityMap.East != nil {
			roads += " north=" + string(*cityMap.East)
		}
		fmt.Printf("%s %s\n", c, roads)
	}
}

