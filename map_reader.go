package main

import (
	"io"
	"io/ioutil"
	"strings"
	"errors"
)

// ReadMap parses input from io.Reader into Map.
//
// The format is the following:
//
// <city name>[ <east|west|south|north>=<city name> ..]
//
// All the referenced cities must be present in the map.
func ReadMap(r io.Reader) (Map, error) {
	bts, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	m, err := parseMap(string(bts))
	if err != nil {
		return nil, err
	}
	err = validateMap(m) // check for missing cities and ambiguous roads
	if err != nil {
		return nil, err
	}
	return m, nil
}

func parseMap(s string) (Map, error) {
	lines := strings.Split(string(s), "\n")

	m := Map{}
	for _, line := range lines {
		tokens := strings.Split(line, " ")
		if tokens[0] == "" {
			continue
		}

		if len(tokens) == 0 {
			// FIXME error codes and directions should be constants
			return nil, errors.New("invalid_format")
		}

		city := City(tokens[0])
		cityMap := CityMap{}
		for _, road := range tokens[1:] {
			roadTokens := strings.Split(road, "=")
			if len(roadTokens) != 2 {
				return nil, errors.New("invalid_format")
			}
			direction, destination := roadTokens[0], roadTokens[1]
			switch direction {
			case "east":
				if cityMap.East != nil {
					return nil, errors.New("ambiguous_roads")
				}
				cityMap.East = NewCity(destination)
			case "west":
				if cityMap.West != nil {
					return nil, errors.New("ambiguous_roads")
				}
				cityMap.West = NewCity(destination)
			case "south":
				if cityMap.South != nil {
					return nil, errors.New("ambiguous_roads")
				}
				cityMap.South = NewCity(destination)
			case "north":
				if cityMap.North != nil {
					return nil, errors.New("ambiguous_roads")
				}
				cityMap.North = NewCity(destination)
			default:
				return nil, errors.New("invalid_direction")
			}
		}

		if _, exists := m[city]; exists {
			return nil, errors.New("same_city_multiple_lines")
		}
		m[city] = cityMap
	}

	if len(m) == 0 {
		return nil, errors.New("empty_map")
	}

	return m, nil
}

func validateMap(m Map) error {

	// FIXME every link is currently checked twice

	for city, cityMap := range m {
		if cityMap.North != nil {
			c, exists := m[*cityMap.North]
			if !exists {
				return errors.New("missing_city")
			}
			if c.South == nil || *c.South != city {
				return errors.New("inconsistent_roads")
			}
		}
	}

	for city, cityMap := range m {
		if cityMap.South != nil {
			c, exists := m[*cityMap.South]
			if !exists {
				return errors.New("missing_city")
			}
			if c.North == nil || *c.North != city {
				return errors.New("inconsistent_roads")
			}
		}
	}

	for city, cityMap := range m {
		if cityMap.West != nil {
			c, exists := m[*cityMap.West]
			if !exists {
				return errors.New("missing_city")
			}
			if c.East == nil || *c.East != city {
				return errors.New("inconsistent_roads")
			}
		}
	}

	for city, cityMap := range m {
		if cityMap.East != nil {
			c, exists := m[*cityMap.East]
			if !exists {
				return errors.New("missing_city")
			}
			if c.West == nil || *c.West != city {
				return errors.New("inconsistent_roads")
			}
		}
	}

	return nil
}
