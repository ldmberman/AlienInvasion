# Simulating the alien invasion

## How it works

### Alien simulation

Each alien is modeled as a goroutine accepting commands from an individual command channel and reporting the result to "radio". All aliens report to the same radio.

Note that it is not strictly necessary to implement aliens as goroutines communicating via channels. I chose this approach solely to demonstrate the channel-based communication. In practice, it might prove useful if the project evolves in the direction of a distributed simulation where aliens are run on different machines.

### Setup

The world map is read from the given file and parsed according to the format.

The map has the form:

```
<city> [<east|north|south|west>=<city> ..]
..
```

It can not be empty. Every road must point at an existing city.

If a city is to the west from some other city then the other city has to be to the east from this city.

`N` aliens are generated in the random places on the map. More than one alien can be generated in the same place.

Every place that contains more than one alien is immediately destroyed, every alien is destroyed, all roads are cut.

### World simulation

The simulation is turn-based.

Each turn, each active alien is given the command to make a turn. Aliens are active for as long as they are not trapped or destroyed.

After all aliens make a turn, the map is checked for cities with more than one alien again. The corresponding cities, aliens, and roads get destroyed. Note that aliens can cross each other without issues. Things only go bad if the aliens **end** their turn in the same city.

Afterwards, alien statuses are updated. If an alien was trapped or destroyed, it is removed from the set of active aliens. If the alien moved, his move count is updated. If the active alien set is empty or all the aliens there moved more than 10000 times, the simulation is over.


## Running the simulation

In the project folder, put a map.txt file containing the world map. Then:

```
go run !(*_test).go <number of aliens>
```

Note that you can't currently run `go run main.go`. To do so, the files need to be put into packages and the project folder has to go to `$GOPATH/src/<import path>` folder. I decided to save some time and not do it this time but in general packages rock!

## Running tests

Enter the project folder and run

```
go test
```
