package main

import (
	"math/rand"
)

type (
	AlienCommand string
	AlienResponse struct {
		ID string
		Response string
		MovedTo *City
	}
)

const (
	AlienTurn = AlienCommand("turn")
	AlienDestroy = AlienCommand("destroy")

	AlienTrapped = "trapped"
	AlienMoved = "moved"
	AlienUnknownCommand = "unknown"
)

// Alien implements a process accepting commands from in and responding to out. The alien described by
// this process begins in the given city. Every time it gets the turn command, it either moves to
// a random neighbouring city or reports to be trapped.
//
// Supported commands are:
//
// turn - commands the alien to make a turn
// destroy - commands the alien to stop operating
//
// Supported responses are:
//
// north - the alien moved north
// east
// west
// south
// trapped - the alien is trapped
// unknown - got unknown command
func Alien(id string, city CityMap, cities Map, in <-chan AlienCommand, out chan<- AlienResponse) {
	myPlace := city

	for {
		command := <-in
		switch command {
		case "turn":
			var response string
			var movedTo *City
			myPlace, movedTo, response = move(myPlace, cities)
			out <- AlienResponse{ID: id, Response: response, MovedTo: movedTo}
		case "destroy":
			return
		default:
			out <- AlienResponse{ID: id, Response: AlienUnknownCommand}
		}
	}
}

func move(city CityMap, cities Map) (destination CityMap, movedTo *City, response string) {
	directions := []*City{city.West, city.North, city.East, city.South}

	existing := 0
	for _, d := range directions {
		if d != nil {
			existing++
		}
	}
	if existing == 0 {
		return city, nil, AlienTrapped
	}

	pick := rand.Int() % existing
	if pick != 0 {
		pick -= 1
	}

	for i, d := range directions {
		if d != nil {
			if pick == 0 {
				neighbour, exists := cities[*d]
				if !exists {
					destination, response = city, AlienTrapped // if the map was parsed properly this will never happen
					return
				}
				destination, movedTo, response = neighbour, directions[i], AlienMoved
				return
			}
			pick--
		}
	}
	return
}
