package main

type City string

func NewCity(s string) *City {
	c := City(s)
	return &c
}

type CityMap struct {
	North *City
	South *City
	West *City
	East *City
}

type Map map[City]CityMap
