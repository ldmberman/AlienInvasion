package main

import (
	"testing"
	"time"
)

type TestAlienTestCase struct {
	Description string

	City CityMap
	Map Map
	ID string
	Command AlienCommand

	ExpectedID string
	ExpectedResponse string
	MovedTo []*City
}

var TestAlienTestCases = []TestAlienTestCase{
	{
		Description: "Unknown command",
		ID: "A",
		City: CityMap{},
		Command: AlienCommand("run"),
		ExpectedID: "A",
		ExpectedResponse: AlienUnknownCommand,
	},

	{
		Description: "Trapped",
		ID: "A",
		City: CityMap{},
		Command: AlienTurn,
		ExpectedID: "A",
		ExpectedResponse: AlienTrapped,
	},

	{
		Description: "Moved north",
		ID: "A",
		City: CityMap{
			North: NewCity("X"),
		},
		Map: Map{
			"X": CityMap{},
		},
		Command: AlienTurn,
		ExpectedID: "A",
		ExpectedResponse: AlienMoved,
		MovedTo: []*City{NewCity("X")},
	},

	{
		Description: "Moved west",
		ID: "A",
		City: CityMap{
			West: NewCity("X"),
		},
		Map: Map{
			"X": CityMap{},
		},
		Command: AlienTurn,
		ExpectedID: "A",
		ExpectedResponse: AlienMoved,
		MovedTo: []*City{NewCity("X")},
	},

	{
		Description: "Moved either south or east",
		ID: "A",
		City: CityMap{
			East: NewCity("X"),
			South: NewCity("Y"),
		},
		Map: Map{
			"X": CityMap{},
			"Y": CityMap{},
		},
		Command: AlienTurn,
		ExpectedID: "A",
		ExpectedResponse: AlienMoved,
		MovedTo: []*City{NewCity("X"), NewCity("Y")},
	},
}

func TestAlien(t *testing.T) {
	for _, testCase := range TestAlienTestCases {

		// To test the pseudorandom aspect of the alien behaviour without mocking the randomisation part,
		// repeat each test case many times (acceptable for the quick unit tests) and assert for the range
		// of possible values.
		for i := 0; i < 100; i++ {
			success := testAlien(t, testCase)
			if !success {
				break
			}
		}
	}
}

func testAlien(t *testing.T, testCase TestAlienTestCase) bool {
	city := testCase.City
	in := make(chan AlienCommand)
	out := make(chan AlienResponse)

	go Alien(testCase.ID, city, testCase.Map, in, out)
	select {
	case in <- testCase.Command:
		break
	case <- time.After(time.Second * 5):
		t.Fatalf("%s: alien did not accept the command within 5 seconds", testCase.Description)
		return false
	}

	select {
	case response := <- out:
		if response.ID != testCase.ExpectedID {
			t.Errorf("%s: bad ID in response expected=%s got=%s", testCase.Description, testCase.ExpectedID, response.ID)
		}
		if response.Response != testCase.ExpectedResponse {
			t.Errorf("%s: got wrong response expected=%s got=%s", testCase.Description, testCase.ExpectedResponse, response.Response)
		}

		if testCase.MovedTo == nil {
			return true
		}

		expected := false
		for _, movedTo := range testCase.MovedTo {
			if movedTo != nil && *movedTo == *response.MovedTo {
				expected = true
				break
			}
		}
		if !expected {
			t.Errorf("%s: moved to wrong city expected to be in %+v got=%s", testCase.Description, testCase.MovedTo, *response.MovedTo)
			return false
		}
	case <- time.After(time.Second * 5):
			t.Fatalf("%s: alien did not respond within 5 seconds", testCase.Description)
	}
	return true
}