package main

import (
	"testing"
	"reflect"
	"strings"
)

type TestReadMapTestCase struct {
	Description string
	Input string

	ExpectedMap Map
	ExpectedErr string
}

var TestReadMapTestCases = []TestReadMapTestCase{
	{
		Description: "Empty map",
		Input: "",
		ExpectedMap: nil,
		ExpectedErr: "empty_map",
	},

	{
		Description: "Empty map, multiple lines",
		Input: `


`,
		ExpectedMap: nil,
		ExpectedErr: "empty_map",
	},

	{
		Description: "Invalid map format",
		Input: "X no idea how to parse this",
		ExpectedMap: nil,
		ExpectedErr: "invalid_format",
	},

	{
		Description: "Invalid direction",
		Input: "X down=Y",
		ExpectedMap: nil,
		ExpectedErr: "invalid_direction",
	},

	{
		Description: "Isolated cities",
		Input: `X
Y
Z`,
		ExpectedMap: Map{
			"X": CityMap{},
			"Y": CityMap{},
			"Z": CityMap{},
		},
		ExpectedErr: "",
	},

	{
		Description: "Connected cities",
		Input: `X east=Y
Y west=X south=Z
Z north=Y`,
		ExpectedMap: Map{
			"X": CityMap{
				East: NewCity("Y"),
			},
			"Y": CityMap{
				West: NewCity("X"),
				South: NewCity("Z"),
			},
			"Z": CityMap{
				North: NewCity("Y"),
			},
		},
		ExpectedErr: "",
	},

	{
		Description: "City described on multiple lines",
		Input: `X east=Y
Y west=X south=Z
Z north=Y
X west=A
A east=X`,
		ExpectedMap: nil,
		ExpectedErr: "same_city_multiple_lines",
	},

	{
		Description: "Ambiguous roads",
		Input: "X west=Y south=Y west=Z",
		ExpectedMap: nil,
		ExpectedErr: "ambiguous_roads",
	},

	{
		Description: "Missing cities",
		Input: `X west=Y`,
		ExpectedMap: nil,
		ExpectedErr: "missing_city",
	},

	{
		Description: "Inconsistent roads",
		Input: `X west=Y
Y east=Z
Z`,
		ExpectedMap: nil,
		ExpectedErr: "inconsistent_roads",
	},
}


func TestReadMap(t *testing.T) {
	for _, testCase := range TestReadMapTestCases {
		expectedMap := testCase.ExpectedMap
		actualMap, err := ReadMap(strings.NewReader(testCase.Input))

		// FIXME failure reports can be made much nicer but you need to write some custom tooling for that
		if !reflect.DeepEqual(actualMap, expectedMap) {
			t.Errorf("%s: maps not equal, expected=%+v got=%+v", testCase.Description, expectedMap, actualMap)
		}

		expectedErr := testCase.ExpectedErr
		if err == nil {
			if expectedErr != "" {
				t.Errorf("%s: expected error did not occur: %s", testCase.Description, expectedErr)
			}
		} else {
			if err.Error() != testCase.ExpectedErr {
				t.Errorf("%s: unexpected err, expected=%s got=%s", testCase.Description, expectedErr, err.Error())
			}
		}
	}
}
