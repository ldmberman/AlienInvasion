package main

import (
	"testing"
	"time"
	"log"
	"reflect"
)

type TestSimulationTestCase struct {
	Description string

	Map Map
	N int64

	ExpectedMaps []Map
}

var TestSimulationTestCases = []TestSimulationTestCase{
	{
		Description: "One city, one alien",
		Map: Map{
			"X": CityMap{},
		},
		N: 1,
		ExpectedMaps: []Map{{
			"X": CityMap{},
		}},
	},

	{
		Description: "One city, two aliens",
		Map: Map{
			"X": CityMap{},
		},
		N: 2,
		ExpectedMaps: []Map{{}},
	},

	{
		Description: "One city, three aliens",
		Map: Map{
			"X": CityMap{},
		},
		N: 3,
		ExpectedMaps: []Map{{}},
	},

	{
		Description: "Two cities not connected, two aliens",
		Map: Map{
			"X": CityMap{},
			"Y": CityMap{},
		},
		N: 2,
		ExpectedMaps: []Map{
			{
				"X": CityMap{}, // 25%
			},
			{
				"Y": CityMap{}, // 25%
			},
			{
				"X": CityMap{},
				"Y": CityMap{},
			},  // 50%, dropped on different cities
		},
	},

	{
		Description: "Two cities connected, two aliens",
		Map: Map{
			"X": CityMap{West: NewCity("Y")},
			"Y": CityMap{East: NewCity("X")},
		},
		N: 2,
		ExpectedMaps: []Map{
			{
				"X": CityMap{}, // 25%
			},
			{
				"Y": CityMap{}, // 25%
			},
			{
				"X": CityMap{West: NewCity("Y")},
				"Y": CityMap{East: NewCity("X")},
			},  // 50%, always cross each other
		},
	},
}

func TestSimulation(t *testing.T) {
	for _, testCase := range TestSimulationTestCases {
		results := make(chan Map, 100)

		// Launch 100 simulations to account for the random nature of it.
		for i := 0; i < 100; i++ {
			go func(r chan<- Map) {

				var m = Map{}
				for k, v := range testCase.Map {
					m[k] = v
				}
				Simulation(m, testCase.N)
				r <- m

			}(results)
		}

		var maps []Map
		read := 100
		for read > 0 {
			select {
			case result := <-results:
				maps = append(maps, result)
				read--
			case <-time.After(time.Second * 5):
				log.Fatalf("%s: a simulation did not finish within 5 seconds", testCase.Description)
			}
		}

		// Each of the expected outcome has to be present in the simulation result list.
		CheckForPossibleResults:
		for _, expectedMap := range testCase.ExpectedMaps {
			for _, actualMap := range maps {
				if reflect.DeepEqual(expectedMap, actualMap) {
					continue CheckForPossibleResults
				}
			}
			t.Errorf("%s: the expected outcome was not produced by any of the 100 simulations %+v", testCase.Description, expectedMap)
		}
	}
}