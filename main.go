package main

import (
	"math/rand"
	"time"
	"os"
	"log"
	"strconv"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())

	// Read the number of aliens to generate from the command line.
	n := readN()

	// Read the map from the file and parse it into Map.
	m := readMap()

	// Run simulation.
	Simulation(m, n)
}

func readN() int64 {
	if len(os.Args) != 2 {
		log.Fatalf("Usage: <program> <integer N>")
	}
	ns := os.Args[1]
	n, err := strconv.ParseInt(ns, 10, 64)
	if err != nil || n < 1 {
		log.Fatalf("Provide a positive integer")
	}
	return n
}

func readMap() Map {
	f, err := os.Open("map.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	m, err := ReadMap(f)
	if err != nil {
		log.Fatal(err)
	}
	return m
}
